<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

if (!Loader::includeModule('sale'))
	return;

$arComponentParameters = Array(
	"PARAMETERS" => array(
		"REDIRECT_URL" => array(
			"NAME" => Loc::getMessage("FO_REDIRECT_PATH"),
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "/personal/cart/",
		),
	),
);

?>