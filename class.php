<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
use Bitrix\Sale;
use Bitrix\Sale\Order;
use Bitrix\Main\Application;

class CBitrixFastOrderComponent extends CBitrixComponent
{

	protected $errorCollection;
	
	public function __construct($component = null)
	{
		parent::__construct($component);
	}

	protected function includeModules()
	{
		$success = true;

		if (!Loader::includeModule('sale'))
		{
			$success = false;
			ShowError(Loc::getMessage('SALE_MODULE_NOT_INSTALL'));
		}

		return $success;
	}

	public function onPrepareComponentParams($params)
	{
		$params["REDIRECT_URL"] = trim((string)$params["REDIRECT_URL"]);
		if (empty($params["REDIRECT_URL"]))
		{
			$params['PATH_TO_BASKET'] = '/personal/cart/';
		}
		return $params;
	}

	public function executeComponent()
	{
		if($this->includeModules())
		{
			CJSCore::Init(array('ajax', 'popup'));
			$basket = Sale\Basket::loadItemsForFUser(\CSaleBasket::GetBasketUserID(), Bitrix\Main\Context::getCurrent()->getSite())->getOrderableItems();
			$basketQntList = $basket->getQuantityList();
			if(!empty($basketQntList))
			{
				$this->arResult["IS_BASKET_EMPTY"] = "N";
			}
			else
			{
				$this->arResult["IS_BASKET_EMPTY"] = "Y";
			}
			$this->IncludeComponentTemplate();
		}
		
	}
}


?>