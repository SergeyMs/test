<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Sale,
    Bitrix\Sale\Order,
    Bitrix\Main\Application,
    Bitrix\Sale\DiscountCouponsManager;

if (!Loader::IncludeModule('sale'))
    die();
$request = Application::getInstance()->getContext()->getRequest();
$phone = $request->getPost("phone");
if (!isset($phone) || !preg_match("/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){12}(\s*)?$/", $phone))
{
    echo json_encode(array(
    	"status" => "N",
        "response" => "Неверный формат телефона"));
    die();
} 
$siteId = \Bitrix\Main\Context::getCurrent()->getSite();
$currencyCode = Option::get('sale', 'default_currency', 'RUB');
$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
$basket->save();
if (CUser::IsAuthorized())
{
    $userID = CUser::getID();
}
else
{
    $userID = CSaleUser::GetAnonymousUserID();
}       
$order = Order::create($siteId, $userID);
$order->setPersonTypeId(1);
$basket = Sale\Basket::loadItemsForFUser(\CSaleBasket::GetBasketUserID(), Bitrix\Main\Context::getCurrent()->getSite())->getOrderableItems();
$order->setBasket($basket);
$basketQntList = $basket->getQuantityList();
$propertyCollection = $order->getPropertyCollection();
$phoneProp = $propertyCollection->getPhone();
$phoneProp->setValue($phone);
if (!empty($basketQntList))
{
    $order->setField('CURRENCY', $currencyCode);
    $order->setField('COMMENTS', 'Заказ оформлен через АПИ. ');
    $result = $order->save();
}
if ($order->getID())
{
    echo json_encode(array(
        "status" => "Y",
        "response" => "Ваш заказ оформлен, номер заказа " . $order->getID()
    ));
    die();
}
else
{
    echo json_encode(array(
        "status" => "N",
        "response" => "Ошибка оформления"
    ));
    die();
}

?>