;(function() {
	'use strict';

	BX.namespace('BX.Sale.BasketComponent');

	BX.Sale.FastOrderComponent = {

	init: function(parameters)
	{
		this.templateFolder = parameters.templateFolder || '';
		this.params = parameters.params;
		this.redirectUrl = this.params.REDIRECT_URL;
		BX.bind(BX("basket-oneclick-button"), 'click', BX.proxy(this.oneClickAсtion, this));
	
	},

	oneClickAсtion: function()
	{
		var ajaxPath = this.templateFolder + "/ajax.php";
			var popup = new BX.PopupWindow('customcomplete', null, {
				closeByEsc: true,
				overlay: true,
				closeIcon: true,
				contentColor: 'white',
				buttons: [
					new BX.PopupWindowButton({
						text: "Оформить заказ в 1 клик",
						className: "btn btn-lg btn-default basket-btn-checkout",
						events: {click: function(){
							BX.ajax.submit(BX("fast-order-form"), function(data){
								var result = JSON.parse(data);
								if (result.status == "Y")
								{
									document.location.href = this.redirectUrl;
									BX.ready(function(){
										alert(result.response);
									})
								}
  							BX("ajax-message").innerHTML = result.response;
							});
						}}
					})
				],
			});
			popup.setContent("<form id='fast-order-form' method='post' action='"+ajaxPath+"'>"+
				"<p>Введите телефон для связи:</p>"+
				"<input type='text' name='phone'>"+
				"</form><div id='ajax-message'></div>");
      		popup.show();
	},

	};

})();



