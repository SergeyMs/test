<?use Bitrix\Main\Localization\Loc;
$this->addExternalJs($templateFolder.'/component.js');
?>



<?if($arResult["IS_BASKET_EMPTY"] == "N"):?>
<button class="btn btn-lg btn-default basket-btn-checkout{{#DISABLE_CHECKOUT}} disabled{{/DISABLE_CHECKOUT}}" 
		id="basket-oneclick-button">
			<?=Loc::getMessage('SBB_ONECLICK')?>
</button>
<?endif;?>

<script type="text/javascript">
	BX.Sale.FastOrderComponent.init({
		result: <?=CUtil::PhpToJSObject($arResult)?>,
		params: <?=CUtil::PhpToJSObject($arParams)?>,
		templateFolder: '<?=CUtil::JSEscape($templateFolder)?>'
	});
</script>
